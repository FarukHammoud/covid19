import 'bootstrap/dist/css/bootstrap.min.css'

import '../css/main.css'

import './icons'
import './check-updates'
import { prepareForm } from './form-util'
import { warnFacebookBrowserUserIfNecessary } from './facebook-util'
import { addVersion } from './util'
import { createForm } from './form'

//gambiarra da braba
import pdfBase from '../certificate.pdf'
import { generatePdf } from './pdf-util'
import { $, $$, downloadBlob } from './dom-utils'
import { last } from 'pdf-lib'

var reasons,address,birthday,city,datesortie,firstname,heuresortie,lastname,placeofbirth,zipcode;

async function createPdf (profile,reasons,pdfBase) {

const pdfBlob = await generatePdf(profile, reasons, pdfBase)

    const creationInstant = new Date()
    const creationDate = creationInstant.toLocaleDateString('fr-CA')
    const creationHour = creationInstant
      .toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })
      .replace(':', '-')

    downloadBlob(pdfBlob, `attestation-${creationDate}_${creationHour}.pdf`)

}
function getInput() {
    const urlParams = new URLSearchParams(window.location.search);
    // http://localhost:1234/?reasons=travail&address=7L Boulevard Jourdan&birthday=20/10/1998&city=Paris&datesortie=31/10/2020&firstname=Faruk&lastname=Hammoud&heuresortie=12:51&placeofbirth=Paranagua&zipcode=75014
    // http://localhost:1234/?reasons=travail&address=7L Boulevard Jourdan&birthday=20/10/1998&city=Paris&firstname=Faruk&lastname=Hammoud&placeofbirth=Paranagua&zipcode=75014
    // http://localhost:1234/?birthday=20/10/1998&firstname=Faruk&lastname=Hammoud&placeofbirth=Paranagua

    reasons = urlParams.get('reasons');

    address = urlParams.get('address');
    birthday = urlParams.get('birthday');
    city = urlParams.get('city');
    datesortie = urlParams.get('datesortie');
    firstname = urlParams.get('firstname');
    heuresortie = urlParams.get('heuresortie');
    lastname = urlParams.get('lastname');
    placeofbirth = urlParams.get('placeofbirth');
    zipcode = urlParams.get('zipcode');

    console.log("get input ended");
    main(2);
}
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
    console.log("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    console.log("Latitude: " + position.coords.latitude + 
    "Longitude: " + position.coords.longitude);
    const Http = new XMLHttpRequest();
    const url = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&featureTypes=&location="+position.coords.longitude+"%2C"+position.coords.latitude;
    Http.open("GET",url,false);
    Http.send();
    //Http.onreadystatechange=(e)=>{
        const obj = JSON.parse(Http.responseText);
        city = obj.address.City
        zipcode = obj.address.Postal
        address = obj.address.Address
        console.log(city,zipcode,address)
        main(3);
    //}
}
function check(){
    
    if (!reasons){
        console.log("no reasons specified.")
        reasons = "sport_animaux";}
    if(!datesortie){
        console.log("no date specified.")
        var creationInstant = new Date()
        var adaptedInstant = new Date(creationInstant-13*60000)
        datesortie = adaptedInstant.toLocaleDateString('fr-CA')
        heuresortie = adaptedInstant.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })
    }
    if (!address){
        console.log("no address specified.")
        getLocation();}
    else{
        main(3);
    }
    console.log("check ended");
}
function main(task){
    if (task==1){
        console.log("task 1")
        getInput();
    }
    if (task==2){
        console.log("task 2")
        check();
    }
    if (task==3){
        console.log("task 3")
        var profile = {"address": address,
        "birthday": birthday,
        "city": city,
        "datesortie": datesortie,
        "firstname": firstname,
        "heuresortie":heuresortie,
        "lastname": lastname,
        "ox-achats": "achats",
        "ox-convocation": "convocation",
        "ox-enfants": "enfants",
        "ox-famille": "famille",
        "ox-handicap": "handicap",
        "ox-missions": "missions",
        "ox-sante": "sante",
        "ox-sport_animaux": "sport_animaux",
        "ox-travail": "travail",
        "placeofbirth": placeofbirth,
        "zipcode": zipcode
        };

        if(firstname){
            console.log(profile,reasons);
            createPdf(profile,reasons,pdfBase);
        }
    }
}

main(1)
warnFacebookBrowserUserIfNecessary()
createForm()
prepareForm()
addVersion(process.env.VERSION)
